use std::io;
use std::io::Write;
use rand::Rng;
use std::cmp::Ordering;


fn secret_number(low: i32, high: i32) -> i32 {
    let secret_number = rand::thread_rng().gen_range(low, high + 1);
    return secret_number;
}

fn compare_and_print(secret: i32, guess: i32) -> bool {
    match guess.cmp(&secret) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("You win !")
    }
    return guess == secret;
}

pub fn run() {
    println!("Game: Guess the number !");

    let secret = secret_number(1, 255);
    let mut guess = String::new();
    let mut trimmed_guess: String;
    let mut round_guessing = 0;
    loop {
        print!("Please input your guess: ");

        io::stdout().flush().expect("Unexpected error while flushing");
        io::stdin().read_line(&mut guess)
            .expect("failed to read line");
        trimmed_guess = guess.trim_end().to_string();
        if trimmed_guess.len().gt(&0) {
            let number: i32 = match trimmed_guess.parse()
            {
                Ok(num) => num,
                Err(_) => {
                    println ! ("Invalid input '{}'", trimmed_guess);
                    continue;
                },
            };
            round_guessing += 1;
            if compare_and_print(secret, number) {
                break;
            }

            guess.clear();
        } else {
            guess.remove(guess.len() - 1);
            println!("Invalid input: '{}'", guess);
        }
    }
    println!("Guessed {} in {} rounds", secret, round_guessing);
}