pub fn shadowing(_oi: i32) {
    let x = 4;
    println!("x: {}", x);
    let x = "Hello";
    println!("x: {}", x);
}

pub fn tuple() {
    let tup: (i32, f64, u8) = (500, 6.4, 1);
    println!("tup destructuring: {} {} {}", tup.0, tup.1, tup.2);
}

pub fn array() {
    let a: [i32; 4] = [1, 2, 3, 4];
    println!("{}", a[3]);
}